# Maintainer: Marcos M. Raimundo <marcosmrai@gmail.com>

# Before building this PKGBUILD, you must download the Gurobi Optimizer
# archive at http://www.gurobi.com/download/gurobi-optimizer and put it
# in the same directory as this PKGBUILD. Registration at gurobi.com is
# required, though free of charge, to be able to download the archive.

pkgname=('gurobi')
_basename=gurobi
pkgver=8.1.1
pkgrel=1
pkgdesc="State-of-the-art solver for mathematical programming"
arch=('x86_64')
url="http://www.gurobi.com/products/gurobi-optimizer"
license=('custom')
depends=('python>=3.6.0' 'python<3.9' 'python2>=2.7.0')
optdepends=(
  'gcc: C/C++ programming interfaces support'
  'java-environment: Java programming interface support'
  'mono: .NET programming interface support'
  'matlab: MATLAB programming interface support, versions 2008b onwards'
)
source=(
#  "${_basename}${pkgver}_linux64.tar.gz::file://${_basename}${pkgver}_linux64.tar.gz"
  "https://packages.gurobi.com/8.1/gurobi8.1.1_linux64.tar.gz"
  "gurobi.sh"
  "gurobi_setup.m"
  "python.3.8.patch"
)
sha256sums=('c030414603d88ad122246fe0e42a314fab428222d98e26768480f1f870b53484'
            '30d535f7100627195dbe8d1c9a5ce603ed645b93eb8869984eb8a15e8db6d1c8'
            'fd328dc00b276258e7828b301c93574f9aa8e6f143caf5428a648851a6ecf93c'
            '4b7de92d025fe152e4f457fb864b60acc4659cec6ff3e23f5dde907d5d0872b9')

prepare() {
  cd "$srcdir/${_basename}${pkgver//./}/linux64/"

  rm bin/python2.7
  rm -r examples/build/

  # Adapt cross-platform scripts to Arch Linux
  cp ${srcdir}/gurobi.sh bin/
  cp ${srcdir}/gurobi_setup.m matlab/

  # patch setup.py for Python 3.8 compatibility
  patch -u setup.py "${srcdir}/python.3.8.patch"
  cp -a lib/python3.7_utf32 lib/python3.8_utf32
}

package_gurobi() {
  install=${_basename}.install

  cd "$srcdir/${_basename}${pkgver//./}/linux64/"

  # License
  install -D -m644 EULA.pdf "${pkgdir}/usr/share/licenses/${_basename}/EULA.pdf"

  # Examples
  install -D -m644 bin/gurobi.env "${pkgdir}/usr/share/${_basename}/gurobi.env"
  cp -r examples/ "${pkgdir}/usr/share/${_basename}/"
  
  # Binaries and related files
  install -D -t "${pkgdir}/usr/bin/" bin/*
  rm "${pkgdir}/usr/bin/gurobi.env"
  install -D -t "${pkgdir}/usr/lib/${_basename}/" lib/gurobi.py

  # Documentation
  install -d "${pkgdir}/usr/share/doc/${_basename}/"
  cp -rT docs/ "${pkgdir}/usr/share/doc/${_basename}/"

  # Headers
  install -D -m644 -t "${pkgdir}/usr/include/" include/*.h

  # Libraries
  install -d "${pkgdir}/usr/lib/"
  cp -d lib/*.so* "${pkgdir}/usr/lib/"
  install -m644 lib/*.a "${pkgdir}/usr/lib/"
  ln -sf ./libgurobi.so.8.1.1 "${pkgdir}/usr/lib/libgurobi.so"
  ln -sf ./libgurobi_g++5.2.a "${pkgdir}/usr/lib/libgurobi_c++.a"

  # Python
  python2 setup.py install --root="$pkgdir" --optimize=1
  python3 setup.py install --root="$pkgdir" --optimize=1

  # Java
  install -D -m644 -t "${pkgdir}/usr/share/java/${_basename}/" lib/gurobi*.jar

  # Matlab
  install -D -t "${pkgdir}/usr/lib/${_basename}/matlab/" matlab/*.mexa64
  install -D -m644 -t "${pkgdir}/usr/lib/${_basename}/matlab/" matlab/*.m
}
